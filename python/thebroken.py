import time

import messages
from race_info import RaceInfo
from car_status import CarStatus
from speedometer import SpeedoMeter

class TheBroken(object):

    def __init__(self):
        self.name = "1872"
        self.color = None

    def _init(self):
        self.race_info = None
        self.car_statuses = {}

        self.speedo = None
        self.crashed = False

    def log(self, log_msg):
        print("{2}".format(self.name, time.strftime('%H:%M:%S'),
                                     log_msg))

    def update_car_statuses(self, data, game_tick):
        for car_status in data:
            car_color = car_status['id']['color']
            last_car_status = self.car_statuses.get(car_color)
            if last_car_status == None:
                self.car_statuses[car_color] = CarStatus(car_status, game_tick)
            else:
                last_car_status.update(car_status, self.race_info, game_tick)

    def get_own_status(self):
        return self.car_statuses.get(self.color)

    def is_quali(self):
        return not self.race_info.laps


    def handle_join(self, data, game_tick):
        self.log("Joined")

    def handle_game_init(self, data, _):
        self._init()
        self.race_info = RaceInfo(data)
        self.log("Received Race Info")

    def handle_game_start(self, data, _):
        if self.speedo is not None:
            kf = self.speedo.kf
            crash_log = self.speedo.crash_log
            self.speedo = SpeedoMeter(self)
            self.speedo.kf = kf
            self.speedo.crash_log = crash_log
            self.speedo.calculate_curve_speeds()
        else:
            self.speedo = SpeedoMeter(self)
        return messages.ping()
        self.log("Race started")

    def handle_yourcar(self, data, _):
        self.name = data['name']
        self.color = data['color']

    def handle_lap_finished(self, data, _):
        car_color = data['car']['color']
        if car_color == self.color:
            lap_time = data['lapTime']['millis']
            race_time = data['raceTime']['millis']

            self.speedo.add_lap_time(lap_time)
            self.speedo.update_racetime(race_time)

            self.log("Lap time: {0}".format(lap_time))
            self.log("Race time: {0}".format(race_time))

    def handle_car_positions(self, data, game_tick):
        self.update_car_statuses(data, game_tick)
        if game_tick:
            response = self.speedo.steer(game_tick)
            return response

    def handle_turbo(self, data, _):
        if not self.crashed:
            self.speedo.add_turbo(data['turboDurationTicks'], data['turboFactor'])

    def handle_turbo_start(self, data, _):
        player = data['color']
        if player == self.color:
            self.speedo.turbo_start()

    def handle_turbo_end(self, data, _):
        player = data['color']
        if player == self.color:
            self.speedo.turbo_end()

    def handle_crash(self, data, _):
        crashed_player = data['color']
        if crashed_player == self.color:
            self.log("I crashed")
            self.crashed = True
            self.speedo.crashed()
        else:
            self.log("{0} crashed".format(crashed_player))

    def handle_spawn(self, data, _):
        spawned_player = data['color']
        if spawned_player == self.color:
            self.log("I spawned")
            self.crashed = False
        else:
            self.log("{0} False".format(crashed_player))

    def handle_game_end(self, data, _):
        self.log("Race ended")

    def handle_error(self, data, _):
        self.log("Error: {0}".format(data))
