import math

class Piece(object): pass


class Straight(Piece):
    def __init__(self, length, switch):
        self.switch = switch
        self._length = length

    def length(self, lane):
        return self._length


class Curve(Piece):
    def __init__(self, radius, angle, switch):
        self.switch = switch
        self.radius = radius
        self.angle = angle

    def length(self, lane):
        act_radius = 0
        if self.angle < 0:
            act_radius = (self.radius + lane)
        else:
            act_radius = (self.radius - lane)
        return (2 * math.pi * act_radius * abs(self.angle)) / 360

    def radius3(self, lane):
        act_radius = 0
        if self.angle < 0:
            act_radius = -(self.radius + lane)
        else:
            act_radius = (self.radius - lane)
        return act_radius


class RaceInfo(object):
    def __init__(self, data):
        self.json_data = data

        race = data['race']
        track = race['track']
        race_session = race['raceSession']

        self.duration_ms = race_session.get('durationMs')

        self.laps = race_session.get('laps')
        self.max_lap_time = race_session.get('maxLapTimeMs')
        self.quick_race = race_session.get('quickRace')

        self.lanes = map(lambda l: l['distanceFromCenter'], track['lanes'])

        self.pieces = []

        for piece_data in track['pieces']:
            switch = piece_data.get('switch', False)
            length = piece_data.get('length')
            if length != None:
                self.pieces.append(Straight(length, switch))
            else:
                radius = piece_data['radius']
                angle = piece_data['angle']
                self.pieces.append(Curve(radius, angle, switch))
