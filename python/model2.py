SANITY_CHECK = False
SANITY_EPS = 10e-7

# Solving for af, df:
#
# s1 = (1 - df) * s0 + af * t0
# s2 = (1 - df) * s1 + af * t1
#
# where:
# s0 = (d1 - d0)
# s1 = (d2 - d1)
# s2 = (d3 - d2)

def _calc_df(d0, d1, d2, d3, t0, t1):
    """Calculate drag factor given observations d0..d3, t0, t1"""
    return (t0 * (d1 - 2*d2 + d3) - t1 * (d0 - 2*d1 + d2)) / (t0 * (d1 - d2) - t1 * (d0 - d1))

def _calc_af(d0, d1, d2, d3, t0, t1):
    """Calculate acceleration factor given observations d0..d3, t0, t1"""
    return (d0*d2 - d0*d3 - d1*d1 + d1*d2 + d1*d3 - d2*d2) / (t0 * (d1 - d2) - t1 * (d0 - d1))


# Solving for t0:
#
# s1 = (1 - df) * s0 + af * t0

def _calc_t0(af, df, s0, s1):
    """Calculate t0 needed to obtain s1 given observations af, df, and s0."""
    return (df * s0 - s0 + s1) / af


class _Observation(object):

    def __init__(self, tick, distance, angle, piece):
        self.tick = tick
        self.distance = distance
        self.angle = angle
        self.piece = piece
        self.throttle = None
        self.speed = None


class Model2(object):

    def __init__(self):
        self.observations = []
        self.af = None
        self.df = None

    def observe(self, tick, distance, angle, piece):
        """Call at the start of a turn"""
        obs = _Observation(tick=tick, distance=distance, angle=angle, piece=piece)
        if self.observations:
            prev_obs = self.observations[-1]
            if SANITY_CHECK:
                assert prev_obs.tick == tick - 1
                if prev_obs.speed is not None:
                    # Measured speed equal to projected speed?
                    assert abs(distance - prev_obs.distance - prev_obs.speed) < SANITY_EPS
            if prev_obs.piece == piece:
                prev_obs.speed = distance - prev_obs.distance
        self.observations.append(obs)
        self._update_af_df()

    def input(self, throttle):
        """Call at the end of a turn"""
        cur_obs = self.observations[-1]
        cur_obs.throttle = throttle

    def current_speed(self):
        """Current projected speed, or None if model not learned"""
        if self.af is None:
            return None
        cur_obs = self.observations[-1]
        prev_obs = self.observations[-2]
        cur_obs.speed = prev_obs.speed * (1 - self.df) + self.af * prev_obs.throttle
        return cur_obs.speed

    def throttle_setting(self, target_speed):
        """Throttle setting for target speed, or None if model not learned"""
        if self.af is None:
            return 1
        ret = _calc_t0(self.af, self.df, self.current_speed(), target_speed)
        return min(max(0, ret), 1)

    def _update_af_df(self):
        if self.af is not None or len(self.observations) < 4:
            return
        window = self.observations[-4:]
        if SANITY_CHECK:
            assert window[1].tick - window[0].tick == 1
            assert window[2].tick - window[1].tick == 1
            assert window[3].tick - window[2].tick == 1
        d0, d1, d2, d3 = [obs.distance for obs in window]
        t0, t1, _,  _  = [obs.throttle for obs in window]
        self.af = _calc_af(d0, d1, d2, d3, t0, t1)
        self.df = _calc_df(d0, d1, d2, d3, t0, t1)
