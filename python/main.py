import sys

import loop
import thebroken


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print("Usage: ./run server port botname botkey")
    else:
        bot = thebroken.TheBroken()
        server, port, bot.name, bot.key = sys.argv[1:5]
        loop.ci_run(bot, server, int(port))
