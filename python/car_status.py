class CarStatus(object):
    def __init__(self, status, game_tick):
        self.json_data = status

        self.speed = 0
        self.accel = 0

        self._set_from_status(status, game_tick)

    def _set_from_status(self, status, game_tick):
        self.tick = game_tick or 0

        pp = status['piecePosition']

        self.lap = pp['lap']

        self.piece_index = pp['pieceIndex']
        self.in_piece_dist = pp['inPieceDistance']

        lane = pp['lane']

        self.start_lane_idx = lane['startLaneIndex']
        self.end_lane_idx = lane['endLaneIndex']

        self.angle = status['angle']

    def update(self, status, race_info, game_tick):
        if game_tick == None:
            return

        # assumption: max one piece boundary crossed since last update
        delta = game_tick - self.tick

        old_speed = self.speed
        npi = status['piecePosition']['pieceIndex']
        nipd = status['piecePosition']['inPieceDistance']

        if npi == self.piece_index:
            self.speed = (nipd - self.in_piece_dist) / delta
        else:
            lane = race_info.lanes[self.start_lane_idx]
            old_piece = race_info.pieces[self.piece_index]
            opl = old_piece.length(lane)
            oipd = self.in_piece_dist
            self.speed = (nipd + opl - oipd) / delta

        self.accel = self.speed - old_speed

        self._set_from_status(status, game_tick)
