import messages
import race_info
import math
from model2 import Model2

from collections import defaultdict


def jc_a2(s,ang):
    return (0.3*s+ang)/(s**2)


# This is *not* the exact model, but should have the same amplitude of oscillations
# x0 - a guess for initial angle
# y0 - a guess for initial angular velocity
def isitsafe(a2val, rightturn, x0, y0, n, s):
    x = x0
    y = y0
    if not rightturn:
        x = -x
        y = -y
    for _ in xrange(n):
        xp = y
        yp = -(0.00125*s + 0.1)*y - 0.00125*s*x + max(a2val * s**2 - 0.3*s, 0)
        x += xp
        y += yp
        if abs(x) >= 59.5:
            return False
    return True

# Given an estimate of safe speed (initial guess/performance only)
# suggest something safer
def safeminmax(bump_ang, bump_speed, r0, x0, y0, rightturn, r, emax):
    a2val = jc_a2(abs(bump_speed), abs(bump_ang)) * math.sqrt(r0 / float(r))
    emin = 0.5656854205 * math.sqrt(r) * 0.8
    emax = float(emax)
    while abs(emax-emin) > 10e-5:
        testp = (emin+emax)/2
        if isitsafe(a2val, rightturn, x0, y0, 1000, testp):
            emin = testp
        else:
            emax = testp
    return emin


def avg(a, b):
    return (a + b) / 2.0


def clamp(a):
    return max(0, min(1, a))



def hyperreal_coast_speed(r):
    return (1.19506541 * 0.5656854205 * math.sqrt(r) + 0.09825875) / 1.1


def final_coast_speed(x1, s0, r0, rn, kf):
    x1 = abs(x1)
    r0 = abs(r0)
    expected_v0 = 0.5656854205 * math.sqrt(r0)
    measured_v0 = (0.3*s0*s0) / (0.3*s0 + x1)
    if x1 < 0.01:
        measured_v0 /= 1.005
    ratio = measured_v0 / expected_v0
    expected_vn = 0.5656854205 * math.sqrt(rn)
    guess1 = ratio * 1.19506541 * expected_vn + 0.09825875
    guess2 = ratio * (1.19506541 * expected_vn + 0.09825875)
    return min(guess1, guess2) / 1.005 * kf


class SpeedoMeter(object):

    def __init__(self, bot):
        self.model = Model2()

        self.race_info = bot.race_info
        self.bot = bot

        self.npieces = len(self.race_info.pieces)

        self.straights = self.calculate_straights()

        self.opti_lanes, self.sw_idxs = self.calculate_opti_switches()
        self.bot.log(str(self.opti_lanes))

        self.last_sw_idx = None

        self.turbo_ticks = 0
        self.turbo_factor = 0
        self.turbo_on = False
        self.turbo_available = False

        self.last_throttle = 1
        self.jc_last_angle = 0

        self.last_piece_idx = None
        self.travel_dist = 0

        self.bump = None
        self.bump_radius = None
        self.bump_speed = None

        self.next_curve_speed = None
        self.prev_speed = 0

        self.kf = 1.0
        self.crash_log = defaultdict(lambda: 0)
        self.curve_speeds = self.calculate_curve_speeds()

        self.race_time = 0
        self.lap_times = []

        self.the_final_straight = False


    def crashed(self):
        own_status = self.bot.get_own_status()
        self.crash_log[own_status.piece_index] += 1
        count = max(self.crash_log.values())
        self.kf = 0.96 ** count
        self.curve_speeds = self.calculate_curve_speeds()


    def steer(self, game_tick):
        own_status = self.bot.get_own_status()
        next_piece_idx = (own_status.piece_index + 1) % self.npieces
        own_piece = self.race_info.pieces[own_status.piece_index]

        if (self.last_piece_idx != None and
            own_status.piece_index != self.last_piece_idx):
            self.travel_dist = self.travel_dist + self.race_info.pieces[
                self.last_piece_idx].length(own_status.start_lane_idx)

        self.last_piece_idx = own_status.piece_index
        ipd = own_status.in_piece_dist

        self.model.observe(game_tick, self.travel_dist + ipd, own_status.angle,
                           own_piece)

        if self.is_turbo_time():
            self.model.input(self.last_throttle)
            self.prev_speed = self.model.current_speed()
            return messages.turbo('zoooooOOooooom..')
        elif (next_piece_idx in self.sw_idxs and
              next_piece_idx != self.last_sw_idx):
            self.last_sw_idx = next_piece_idx

            sw_idx = self.sw_idxs.index(next_piece_idx)
            opti_lane = self.opti_lanes[sw_idx]

            own_lane = self.race_info.lanes[own_status.start_lane_idx]

            if opti_lane > own_lane:
                self.model.input(self.last_throttle)
                return messages.switch_lane('Right')
            elif opti_lane < own_lane:
                self.model.input(self.last_throttle)
                return messages.switch_lane('Left')

        self.last_throttle = self.calculate_throttle(game_tick)
        self.prev_speed = self.model.current_speed()

        self.model.input(self.last_throttle)
        return messages.throttle(self.last_throttle)

    def get_finish_straight(self):
        for s in self.straights:
            if 0 in s:
                return s

    def get_longest_straights(self):
        l = None
        ls = []
        for s in self.straights:
            if l is None or len(s) == l:
                l = len(s)
                ls.append(s)
        return ls

    def get_next_longest_straight(self):
        own_status = self.bot.get_own_status()
        pi = own_status.piece_index

        ls = self.get_longest_straights()
        return min(ls, key=lambda s: (s[0] - pi) % self.npieces)

    def is_turbo_time(self):
        own_status = self.bot.get_own_status()
        return (self.turbo_available and
                (len(self.straights) != 0 and
                 own_status.piece_index ==
                 self.get_next_longest_straight()[0]))

    def update_racetime(self, race_time):
        self.race_time = race_time

    def add_lap_time(self, lap_time):
        self.lap_times.append(lap_time)

    def final_straight(self):
        finish_straight = self.get_finish_straight()
        if finish_straight:
            pfls_pieces = finish_straight[0:finish_straight.index(0)]

            own_status = self.bot.get_own_status()
            pi = own_status.piece_index

            conditions_met = False
            if self.bot.is_quali():
                if len(self.lap_times) >= 1:
                    race_duration = self.race_info.duration_ms
                    time_remaining = race_duration - self.race_time
                    self.bot.log('Time remaining: {0}'.format(time_remaining))

                    lap_times = list(self.lap_times)
                    lap_times.sort()

                    lt = lap_times[int(len(lap_times) / 2)]

                    conditions_met = time_remaining - lt < lt
            else:
                laps = self.race_info.laps
                current_lap = own_status.lap

                conditions_met = current_lap + 1 == laps

            if conditions_met and pi in pfls_pieces:
                self.bot.log('On the Final Straight!')
                self.the_final_straight = True

    def is_break_time(self, speed, throttle, curve_speed, d_to_curve):
        if not self.the_final_straight:
            self.final_straight()

        if self.the_final_straight:
            return False

        model = self.model

        d_to_curve = (d_to_curve - (1 - model.df) * speed - model.af *
                      (self.turbo_on and self.turbo_factor * throttle or throttle))
        while speed > curve_speed:
            speed = speed * (1 - model.df)
            d_to_curve = d_to_curve - speed

        return d_to_curve <= 0

    def calc_curve_speed(self, p, lane):
        rad = p.radius
        if p.angle < 0:
            rad = rad + lane
        else:
            rad = rad - lane
        curve_speed = 0
        if self.bump:
            curve_speed = final_coast_speed(self.bump,
                                            self.bump_speed,
                                            self.bump_radius,
                                            rad,
                                            self.kf)
        else:
            curve_speed = hyperreal_coast_speed(rad)
        return curve_speed

    def calculate_curve_speeds(self):
        pieces = self.race_info.pieces
        np = self.npieces

        own_status = self.bot.get_own_status()
        own_piece_idx = own_status.piece_index
        own_piece = pieces[own_piece_idx]
        own_lane = self.race_info.lanes[own_status.start_lane_idx]

        angle = own_status.angle

        curve_speeds = {}

        pi = (own_piece_idx + 1) % np
        lane = own_lane
        while pi != own_piece_idx:
            p = pieces[pi]
            if pi in self.sw_idxs:
                lane = self.opti_lanes[self.sw_idxs.index(pi)]
            if type(p) is race_info.Curve:
                curve_speeds[pi] = self.calc_curve_speed(p, lane)
            pi = (pi + 1) % np
        if type(own_piece) is race_info.Curve:
            curve_speeds[own_piece_idx] = self.calc_curve_speed(own_piece, lane)

        return curve_speeds

    def calculate_curve_distances(self):
        np = self.npieces
        pieces = self.race_info.pieces

        own_status = self.bot.get_own_status()
        own_piece_idx = own_status.piece_index
        own_piece = pieces[own_piece_idx]
        own_lane = self.race_info.lanes[own_status.start_lane_idx]

        curve_dists = {}

        pi = (own_piece_idx + 1) % np
        lane = own_lane
        dtc = pieces[own_piece_idx].length(lane) - own_status.in_piece_dist
        while pi != own_piece_idx:
            piece = pieces[pi]
            if type(piece) is race_info.Curve:
                curve_dists[pi] = dtc
            if pi in self.sw_idxs:
                lane = self.opti_lanes[self.sw_idxs.index(pi)]
            dtc = dtc + piece.length(lane)
            pi = (pi + 1) % np
        if type(own_piece) is race_info.Curve:
            curve_dists[own_piece_idx] = dtc

        return curve_dists

    def calculate_throttle(self, game_tick):
        logstr = ""

        if self.model.current_speed() is None:
            return self.last_throttle

        pieces = self.race_info.pieces
        own_status = self.bot.get_own_status()
        own_piece_idx = own_status.piece_index
        own_piece = pieces[own_piece_idx]

        speed = own_status.speed
        accel = own_status.accel
        angle = own_status.angle

        if angle and not self.bump:
            lane = self.race_info.lanes[own_status.start_lane_idx]
            self.bump = angle
            if own_piece.angle < 0:
                self.bump_radius = own_piece.radius + lane
            else:
                self.bump_radius = own_piece.radius - lane
            self.bump_speed = self.prev_speed
            self.curve_speeds = self.calculate_curve_speeds()

        curve_dists = self.calculate_curve_distances()

        # check across all curves if we have to break
        break_curves = []
        for ci, cs in self.curve_speeds.iteritems():
            dtc = curve_dists[ci]
            if self.is_break_time(speed, self.last_throttle, cs, dtc):
                break_curves.append(ci)

        if len(break_curves) > 0:
            next_throttle = 0
        elif type(own_piece) is race_info.Curve:
            curve_speed = self.curve_speeds[own_piece_idx]
            jc_rightturn = own_piece.angle > 0
            if self.bump:
                jc_curve_speed = safeminmax(self.bump,
                                            self.bump_speed,
                                            self.bump_radius,
                                            angle,
                                            angle - self.jc_last_angle,
                                            jc_rightturn,
                                            own_piece.radius,
                                            curve_speed)
                if curve_speed - jc_curve_speed > 0.01:
                    logstr += "cs: %.3f -> %.3f" % (curve_speed, jc_curve_speed)
                    curve_speed = jc_curve_speed
            next_throttle = self.model.throttle_setting(curve_speed)
        else:
            next_throttle = 1


        strrad = ""
        if type(own_piece) == race_info.Curve:
            jc_rad = own_piece.radius
            if own_piece.angle < 0:
                jc_rad = jc_rad * -1
            strrad = "  rad=%d" % jc_rad

        statusstr = "t=%d  d=%d/%.2f  spd=%.3f  ang=%.4f  nxt=%.4f%s" % (game_tick, own_status.piece_index, own_status.in_piece_dist, speed, angle, next_throttle, strrad)
        if logstr != "":
            logstr = statusstr + "  |  " + logstr
        else:
            logstr = statusstr
        self.bot.log(logstr)

        self.jc_last_angle = angle
        self.last_throttle = next_throttle
        return next_throttle

    def calculate_straights(self):
        pieces = self.race_info.pieces
        np = self.npieces

        i = 0
        while type(pieces[i]) is race_info.Straight:
            i = (i - 1) % np
        i = (i + 1) % np
        marked = set()
        straights = []
        current_straight = []
        while i not in marked:
            marked.add(i)
            p = pieces[i]
            if type(p) is not race_info.Straight:
                if len(current_straight) != 0:
                    straights.append(current_straight)
                current_straight = []
            else:
                current_straight.append(i)
            i = (i + 1) % np
        self.bot.log(straights)
        straights.sort(key=lambda s: len(s), reverse=True)
        self.bot.log(straights)
        return straights

    def calculate_opti_switches(self):
        race_info = self.race_info
        pieces = race_info.pieces
        lanes = race_info.lanes
        np = self.npieces

        sw_idxs = [i for i, p in enumerate(pieces) if p.switch]

        mlanes = []
        l_idx = sw_idxs[0]
        last_m_l = None
        for sw_idx in sw_idxs[1:] + [sw_idxs[0]]:
            btw_ps = []
            i = (l_idx + 1) % np
            while i != sw_idx:
                btw_ps.append(pieces[i])
                i = (i + 1) % np
            m_l = last_m_l
            m = None
            if m_l is not None:
                m = sum(p.length(last_m_l) for p in btw_ps)
            for l in lanes:
                ll = sum(p.length(l) for p in btw_ps)
                if m_l == None or ll < m:
                    m = ll
                    m_l = l
            last_m_l = m_l
            l_idx = sw_idx
            mlanes.append(m_l)
        return mlanes, sw_idxs

    def add_turbo(self, turbo_ticks, turbo_factor):
        self.turbo_ticks = turbo_ticks
        self.turbo_factor = turbo_factor
        self.turbo_available = True

    def turbo_start(self):
        self.turbo_on = True
        self.turbo_available = False
        self.bot.log('turbo started')

    def turbo_end(self):
        self.turbo_on = False
        self.bot.log('turbo ended')
