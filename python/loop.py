import json
import socket
import time

from messages import ping

def _bot_id(bot):
    return {
        'name': bot.name,
        'key': bot.key
    }


def _track_params(bot, cars, track):
    return {
        'botId': _bot_id(bot),
        'trackName': track,
        'password': '3RlnxJXV',
        'carCount': cars
    }


def _connection_sanity(bot, server, port):
    print("Connecting: server={0} port={1} name={2} key={3}".format(
        server,
        port,
        bot.name,
        bot.key))


def _make_socket(server, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((server, port))
    return sock


def _send(sock, msg_type, data, game_tick=-1):
    str = json.dumps({'msgType': msg_type, 'data': data, 'gameTick': game_tick})
    sock.sendall(str + '\n')


def _loop(sock, bot):
    msg_map = {
        'join': bot.handle_join,
        'gameInit': bot.handle_game_init,
        'gameStart': bot.handle_game_start,
        'carPositions': bot.handle_car_positions,
        'crash': bot.handle_crash,
        'gameEnd': bot.handle_game_end,
        'turboAvailable': bot.handle_turbo,
        'turboStart': bot.handle_turbo_start,
        'turboEnd': bot.handle_turbo_end,
        'error': bot.handle_error,
        'lapFinished': bot.handle_lap_finished,
        'yourCar': bot.handle_yourcar
    }
    socket_file = sock.makefile()
    line = socket_file.readline()
    while line:
        msg = json.loads(line)
        msg_type, data, game_tick = msg['msgType'], msg['data'], msg.get('gameTick')
        if msg_type in msg_map:
            result = msg_map[msg_type](data, game_tick)
            if (result is not None):
                resp_type, resp_data = result
                _send(sock, resp_type, resp_data, game_tick)
        else:
            print("[{0}] Got {1}".format(time.strftime('%H:%M:%S'), msg_type))
        line = socket_file.readline()


def ci_run(bot, server, port):
    _connection_sanity(bot, server, port)
    sock = _make_socket(server, port)
    _send(sock, 'join', _bot_id(bot))
    _loop(sock, bot)


def test_run(bot, cars=1, track=None, host=False, name=None):
    """Use it like:

    test_run(bot)
    test_run(bot, track='keimola')
    test_run(bot, track='keimola', cars=2, host=True)
    test_run(bot, track='keimola', cars=2)
    """
    assert (not host) or (cars > 1)
    assert (cars == 1) or track
    bot.key = 'S3jSoiH68RFSXg'
    if name:
        bot.name = name
    server = "hakkinen.helloworldopen.com"
    bot.jc_server = server.split('.')[0]
    bot.jc_track = track or 'keimola'
    port = 8091
    _connection_sanity(bot, server, port)
    sock = _make_socket(server, port)
    if cars == 1 and not track:
        _send(sock, 'join', _bot_id(bot))
    elif host or cars == 1:
        _send(sock, 'createRace', _track_params(bot, cars, track))
    else:
        _send(sock, 'joinRace', _track_params(bot, cars, track))
    _loop(sock, bot)
